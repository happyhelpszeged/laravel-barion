<?php

Route::group(['namespace' => 'AtiHH\Barion\Http\Controllers', 'prefix' => 'barion/atihh', 'middleware' => ['web']], function(){
    Route::get('/', 'BarionController@index');
});
