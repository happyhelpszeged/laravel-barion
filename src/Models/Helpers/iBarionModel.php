<?php

namespace AtiHH\Barion\Models\Helpers;

interface iBarionModel
{
    public function fromJson($json);
}
