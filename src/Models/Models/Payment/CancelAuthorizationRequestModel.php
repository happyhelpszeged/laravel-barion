<?php


namespace AtiHH\Barion\Models\Models\Payment;


use AtiHH\Barion\Models\Models\BaseRequestModel;

class CancelAuthorizationRequestModel extends BaseRequestModel
{
    public $PaymentId;

    function __construct($paymentId)
    {
        $this->PaymentId = $paymentId;
    }
}
