<?php


namespace AtiHH\Barion\Models\Models\Payment;


use AtiHH\Barion\Models\Common\QRCodeSize;
use AtiHH\Barion\Models\Models\BaseRequestModel;

class PaymentQRRequestModel extends BaseRequestModel
{
    public $UserName;
    public $Password;
    public $PaymentId;
    public $Size;

    function __construct($paymentId)
    {
        $this->PaymentId = $paymentId;
        $this->Size = QRCodeSize::Normal;
    }
}
