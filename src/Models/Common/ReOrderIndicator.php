<?php


namespace AtiHH\Barion\Models\Common;


abstract class ReOrderIndicator
{
    const FirstTimeOrdered = "FirstTimeOrdered";
    const ReOrdered = "ReOrdered";
}
