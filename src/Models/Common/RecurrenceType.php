<?php


namespace AtiHH\Barion\Models\Common;


abstract class RecurrenceType
{
    const MerchantInitiatedPayment = "MerchantInitiatedPayment";
    const OneClickPayment = "OneClickPayment";
}
