<?php


namespace AtiHH\Barion\Models\Common;


abstract class SuspiciousActivityIndicator
{
    const NoSuspiciousActivityObserved = "NoSuspiciousActivityObserved";
    const SuspiciousActivityObserved = "SuspiciousActivityObserved";
}
