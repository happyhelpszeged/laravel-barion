<?php


namespace AtiHH\Barion\Models\Common;


abstract class RecurrenceResult
{
    const None = "None";
    const Successful = "Successful";
    const Failed = "Failed";
    const NotFound = "NotFound";
}
