<?php


namespace AtiHH\Barion\Models\Common;


abstract class QRCodeSize
{
    const Small = "Small";
    const Normal = "Normal";
    const Large = "Large";
}
