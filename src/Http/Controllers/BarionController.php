<?php


namespace AtiHH\Barion\Http\Controllers;


use App\Http\Controllers\Controller;
use AtiHH\Barion\Models\BarionClient;
use AtiHH\Barion\Models\Common\BarionEnvironment;
use AtiHH\Barion\Models\Common\Currency;
use AtiHH\Barion\Models\Common\FundingSourceType;
use AtiHH\Barion\Models\Common\PaymentType;
use AtiHH\Barion\Models\Common\UILocale;
use AtiHH\Barion\Models\Models\Common\ItemModel;
use AtiHH\Barion\Models\Models\Payment\PaymentTransactionModel;
use AtiHH\Barion\Models\Models\Payment\PreparePaymentRequestModel;
use AtiHH\Barion\Models\Models\ThreeDSecure\ShippingAddressModel;

class BarionController extends Controller
{
    public function index(){

        $myPosKey = "";
        $myEmailAddress = "info@happyhelp.hu";

        $BC = new BarionClient($myPosKey, 2, BarionEnvironment::Test);

        // create the item model
        $item = new ItemModel();
        $item->Name = "TestItem"; // no more than 250 characters
        $item->Description = "A test item for payment"; // no more than 500 characters
        $item->Quantity = 1;
        $item->Unit = "piece"; // no more than 50 characters
        $item->UnitPrice = 1000;
        $item->ItemTotal = 1000;
        $item->SKU = "ITEM-01"; // no more than 100 characters

        // create the transaction
        $trans = new PaymentTransactionModel();
        $trans->POSTransactionId = "TRANS-01";
        $trans->Payee = $myEmailAddress; // no more than 256 characters
        $trans->Total = 1000;
        $trans->Comment = "Test Transaction"; // no more than 640 characters
        $trans->AddItem($item); // add the item to the transaction

        // create the shipping address
        $shippingAddress = new ShippingAddressModel();
        $shippingAddress->Country = "HU";
        $shippingAddress->Region = null;
        $shippingAddress->City = "Szeged";
        $shippingAddress->Zip = "1111";
        $shippingAddress->Street = "Teszt utca 1.";
        $shippingAddress->Street2 = "1. emelet 1. ajto";
        $shippingAddress->Street3 = "";
        $shippingAddress->FullName = "Teszt Elek";

        // create the request model
        $ppr = new PreparePaymentRequestModel();
        $ppr->GuestCheckout = true;
        $ppr->PaymentType = PaymentType::Immediate;
        $ppr->FundingSources = array(FundingSourceType::All);
        $ppr->PaymentRequestId = "PAYMENT-01";
        $ppr->PayerHint = "user@example.com";
        $ppr->Locale = UILocale::HU;
        $ppr->OrderNumber = "ORDER-0001";
        $ppr->Currency = Currency::HUF;
        $ppr->RedirectUrl = "http://webshop.example.com/afterpayment";
        $ppr->CallbackUrl = "http://webshop.example.com/processpayment";
        $ppr->AddTransaction($trans);

        // send the request
        $myPayment = $BC->PreparePayment($ppr);

        if ($myPayment->RequestSuccessful === true) {
            return redirect()->to(BARION_WEB_URL_TEST."?id=" . $myPayment->PaymentId);
        }
    }
}
